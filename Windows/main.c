// Including the necessary libraries
#include <stdio.h>
#include <stdbool.h>
#include <time.h>

// Defining constants
#define DEFAULT_HEIGHT 6
#define DEFAULT_WIDTH 7
#define HEIGHT_START 0
#define WIDTH_START 0
#define MAX_ROUNDS DEFAULT_HEIGHT*DEFAULT_WIDTH

#define MODE_HUMAN_HUMAN 1
#define MODE_COMPUTER_HUMAN 2
#define MODE_REPLAY 3

#define COMPUTER_PLAYER_ID 2

// Initialising mode
int mode;

// Declaring frequently used variables
int height;
int width;

// Setting Player 1 to be active for the start
int player_id=1;
int winning_player;

// Setting up variables for board history storage to allow for undo and redo
int round_num;
int highest_round_num;
int last_col_played;
bool added;

// Creating the board as a 2D array
int board[DEFAULT_HEIGHT][DEFAULT_WIDTH];

// Creating board history storage as a 3D array
int board_history[MAX_ROUNDS][DEFAULT_HEIGHT][DEFAULT_WIDTH];

int main(void)
{   
    // Declaring variables that require considerable memory use
    char* input_char;
    int input;

    // Declaring methods used within the main method
    void start_game(char* input_char);
    void play_round(char* input_char, int input);
    int check_win(int player);
    void display_win(int winning_player);
    int get_winning_player();
    int get_mode();
    void end_game(char* input_char);
    void computer_round();
    void replay_board_history(char* input_char);
    void delay(int milliseconds);

    // Starting the game
    start_game(input_char);

    // Playing rounds until a player has won
    while(!check_win(1) && !check_win(2)) {
        if(get_mode()==MODE_COMPUTER_HUMAN)
        {
            // Computer round will alternate between human and computer
            play_round(input_char, input);
            computer_round();
        }
        else if(get_mode()==MODE_REPLAY)
        {
            // Replay will run the replay function
            replay_board_history(input_char);
        }
        else
        {
            // Human vs human mode will loop a player being allowed to input a value
            play_round(input_char, input);
        }
    }

    // Displaying the winning player's win once they've qualified
    end_game(input_char);

    // Terminating program once a win has been reached
    delay(5000);
    return 0;
}

// Getters and Setters for frequently used variables

int get_winning_player()
{
    return winning_player;
}

void set_winning_player(int player)
{
    winning_player=player;
}

// Allowing for an adjustment to the result as this is frequently required
int get_round_num(int adj)
{
    return round_num+adj;
}

void set_round_num(int num)
{
    round_num=num;
}

int get_highest_round_num()
{
    return highest_round_num;
}

void set_highest_round_num(int num)
{
    highest_round_num=num;
}

int get_mode()
{
    return mode;
}

void set_mode(int num)
{
    mode=num;
}

int get_player_id()
{
    return player_id;
}

void set_player_id(int id) 
{
    player_id=id;
}

int get_board_value(int ver, int hor)
{
    return board[ver][hor];
}

void set_board_value(int ver, int hor, int value)
{
    board[ver][hor]=value;
}

int get_last_col_played()
{
    return last_col_played;
}

void set_last_col_played(int col)
{
    last_col_played=col;
}

int get_board_history_value(int rnd, int ver, int hor)
{
    return board_history[rnd][ver][hor];
}

void set_board_history_value(int rnd, int ver, int hor, int value)
{
    board_history[rnd][ver][hor]=value;
}

bool is_added()
{
    return added;
}

void set_added(bool val)
{
    added=val;
}

// Used to create a delay within loops
void delay(int milliseconds)
{
    int mil_second=1000*milliseconds;
    clock_t start_time=clock();
    while (clock()<start_time + mil_second);
}

// Printing the board array graphically in the terminal
void show_board()
{
    // Creating space
    printf("\n");
    printf("%s", "       ");

    for(width=WIDTH_START; width<DEFAULT_WIDTH; width++)
    {
        // Printing the column values with equal spaces
        printf("%i %s", width+1, "  ");
    }

    printf("%s", "\n\n\n");

    for(height=0; height<DEFAULT_HEIGHT; height++) 
    {

        // Printing the row values at the start of each one
        printf("%i ", height+1);
        printf("%s", "     ");
        
        for(width=0; width<DEFAULT_WIDTH; width++) 
        {
            // Priting all board values out
            printf("%i   ", get_board_value(height, width));
        }
        printf("\n\n");
    }
}

void start_game(char * input_char) 
{
    int input=0;

    puts("Welcome to Connect-4 \n");
    
    // Increasing the round number
    set_round_num(get_round_num(1));

    // Increasing the ceiling of the highest round reached for accurate redos
    set_highest_round_num(get_round_num(0));

    // Starting with a mode options menu
    while(get_mode()!=1 && get_mode()!=2 && get_mode()!=3)
    {
        puts("Please pick your mode: \n1) Human vs Human \n2) Human vs Computer \n3) Game Replay\n");
        fgets(input_char, 20, stdin);
        sscanf(input_char, "%d", &input);

        if(input>0 && input<=3)
        {
            set_mode(input);
        }
        else
        {
            puts("Please select a valid option!\n");
        }
    }

    puts("\nLET THE GAME BEGIN! \n\n");
}

// Used between rounds to switch the player
void change_player()
{
    if(get_player_id()==1) 
    {
        set_player_id(2);
    }
    else
    {
        set_player_id(1);
    }
}

// Used for changing the board when an undo or redo occurs
void replace_board()
{
    for(height=HEIGHT_START; height<DEFAULT_HEIGHT; height++)
    {
        for(width=WIDTH_START; width<DEFAULT_WIDTH; width++)
        {
            set_board_value(height, width, get_board_history_value(get_round_num(-1), height, width));
        }
    }

    change_player();
    show_board();
}

// Undo the board by one
void undo(bool flag)
{
    if(get_round_num(0)>1)
    {
        // Decrease the round number
        set_round_num(get_round_num(-1));
        replace_board();
    }
    else
    {
        // Some undos should not make this announcement
        if(flag)
        {
            puts("You cannot undo any more moves! \n");
        }
    }
}

// Redo the board by one
void redo(bool flag)
{
    if(get_round_num(0)<get_highest_round_num())
    {   
        // Increase the round number 
        set_round_num(get_round_num(1));
        replace_board();      
    }
    else
    {
        // Some redos should not make this announcement
        if(flag)
        {
            puts("You cannot redo any more moves! \n");
        }
    }
}

void start_round() 
{
    printf("%s %i %s", "Player", get_player_id(), "- choose your column (1-7) - type '88' to undo move - type '99' to redo move\n");
}

// Record the current board into the round_num position of the board_history array
void add_board_history()
{
    for(height=HEIGHT_START; height<DEFAULT_HEIGHT; height++)
    {
        for(width=WIDTH_START; width<DEFAULT_WIDTH; width++)
        {
            // Copies an instance of the entire board array to a position within the board_history one
            set_board_history_value(get_round_num(0), height, width, get_board_value(height, width));
        }
    }
}

// Handles the gameplay of a single round
void play_round(char* input_char, int input)
{
    // Declaring necessary variables
    int row=DEFAULT_HEIGHT;
    bool valid_input;
    set_added(false);

    show_board();

    // Runs in a loop until a valid input has been used
    while(valid_input==false)
    {
        start_round();
        
        if(get_mode()==MODE_HUMAN_HUMAN || get_player_id()!=COMPUTER_PLAYER_ID)
        {
            input=0;
        }

        // Gets user input and converts it to an int
        if(!input)
        {
            fgets(input_char, 20, stdin);
            sscanf(input_char, "%d", &input);
        }

        if(input>0 && input<=DEFAULT_WIDTH)
        {
            valid_input=true;

            while(is_added()==false && row>0)
            {
                // Ensures the space on the board is empty before adding entry
                if(get_board_value(row-1, input-1)==0)
                {
                    set_board_value(row-1, input-1, get_player_id());                    
                    set_added(true);
                    set_last_col_played(input);

                    if(get_mode()==MODE_COMPUTER_HUMAN && get_player_id()==COMPUTER_PLAYER_ID)
                    {
                        printf("\n\n%s %i\n", "COMPUTER ADDED A TOKEN IN COLUMN ", input);
                    }

                    change_player();
                    add_board_history();
                    set_round_num(get_round_num(1));
                    set_highest_round_num(get_round_num(0));
                }
                // row decreases for each time a vertical stack has an entry
                row--;
            }
            // Alerts user if a column has no empty spaces
            if(get_board_value(HEIGHT_START, input-1) !=0 && is_added()==false)
            {
                puts("COLUMN IS FULL! \n");
            }
        }
        else if(input==88)
        {
            undo(true);

            if(get_mode()==MODE_COMPUTER_HUMAN)
            {
                // Will undo twice if playing against a computer, but won't announce if impossible twice
                undo(false);
            }
        }
        else if(input==99)
        {
            redo(true);

            if(get_mode()==MODE_COMPUTER_HUMAN)
            {
                // Will redo twice if playing against a computer, but won't announce if impossible twice
                redo(false);
            }
        }
        else
        {
            puts("PLEASE CHOOSE A COLUMN FROM 1-7! \n");
        } 
    }
    input=0;
}

// Checks for four entries of the same player across
bool check_horizontal(int player, int row, int column) 
{
    // Also ensures that the selection doesn't extend the bounds of the board
    if(get_board_value(row, column)>WIDTH_START && get_board_value(row, column+1)==player && get_board_value(row, column+2)==player && get_board_value(row, column+3)==player && (column+3)<DEFAULT_WIDTH)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// Checks for four entries of the same player up and down
bool check_vertical(int player, int row, int column) 
{
    // Also ensures that the selection doesn't extend the bounds of the board
    if(get_board_value(row+1, column)==player && get_board_value(row+2, column)==player && get_board_value(row+3, column)==player && (row+3)<DEFAULT_HEIGHT)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// Checks for four entries of the same player diagonally upwards
bool check_diagonal_up(int player, int row, int column) 
{
    // Also ensures that the selection doesn't extend the bounds of the board
    if(get_board_value(row+1, column-1)==player && get_board_value(row+2, column-2)==player && get_board_value(row+3, column-3)==player && (row+3)<DEFAULT_HEIGHT && (column-3)>=WIDTH_START)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// Checks for four entries of the same player diagonally downwards
bool check_diagonal_down(int player, int row, int column) 
{
    // Also ensures that the selection doesn't extend the bounds of the board
    if(get_board_value(row+1, column+1)==player && get_board_value(row+2, column+2)==player && get_board_value(row+3, column+3)==player && (column+3)<DEFAULT_WIDTH && (row+3)<DEFAULT_HEIGHT)
    {
        return true;
    }
    else
    {
        return false;
    }
}

// Checks if a specified player has won a round
int check_win(int player)
{
    set_winning_player(0);

    for(height=HEIGHT_START; height<DEFAULT_HEIGHT; height++)
    {
        for(width=WIDTH_START; width<DEFAULT_WIDTH; width++)
        {
            // Only fully checks for a match once a player's entry has been found
            if(get_board_value(height, width)==player)  
            {
                // Giving each option a different line for better readability
                if
                (
                    check_horizontal(player, height, width) ||
                    check_vertical(player, height, width) ||
                    check_diagonal_up(player, height, width) ||
                    check_diagonal_down(player, height, width)
                )
                {
                    set_winning_player(player);

                    // Returning winning player here to stop the loop when a match is found
                    return get_winning_player();
                }
            }
        }
    }
    // Creates a virtual player 3 in the case of a full board which is a tie
    if (get_round_num(-1)==MAX_ROUNDS)
    {
        set_winning_player(3);
        return get_winning_player();
    }
    // Default option
    return false;
}

// Announces the winning player
void display_win(int winning_player)
{
    // Only displays a win if the there's a winning player
    if(winning_player)
    {
        show_board();

        if(winning_player==3)
        {
            // Announce if it's a tie
            puts("IT'S A TIE! NO ONE WINS!");
        }
        else
        {
        printf("%s %i %s", "PLAYER ", winning_player, " WINS! \n");
        }
    }
}

// Used to save the game for later replay
void handle_save(char * input_char)
{
    int input=0;

    while(input!=1 && input!=999)
        {
            puts("\nWould you like to save this game? - type 1 for YES - type 999 for NO");
            fgets(input_char, 20, stdin);
            sscanf(input_char, "%d", &input);

            if(input!=1 && input!=999)
            {
                puts("Please choose a valid option");
            }
        }

        if(input==1)
        {
            // The game saves to a specified location
            FILE *board_history_file_save;
            remove(".\\save\\board_history.bin");
            board_history_file_save = fopen(".\\save\\board_history.bin","wb");
            fwrite(board_history,sizeof(board_history),1,board_history_file_save);
            puts("\nGAME SAVED TO '.\\save\\board_history.bin'");
        }
}

// Handles the end of the game after a player has won
void end_game(char * input_char)
{
    display_win(get_winning_player());

    // Handling the saving
    if(get_mode()==MODE_HUMAN_HUMAN || get_mode()==MODE_COMPUTER_HUMAN)
    {
        // Only offering save if a game was actually played, not replayed
        handle_save(input_char);
    }

    puts("\nGoodbye! Thanks for playing!\n");
}

void computer_round() 
{
    // A relatively randomly selected value for the computer to pick as its column to play
    int column_to_play=(get_round_num(0)+get_round_num(get_last_col_played()))+get_last_col_played();
    int row=WIDTH_START;
    set_added(false);

    // Only runs if the correct player is active
    if(get_player_id()==COMPUTER_PLAYER_ID) {
        while(column_to_play>DEFAULT_WIDTH || column_to_play<=WIDTH_START)
        {
            // Until the column_to_play value is within range of the board, it will reduce
            column_to_play=(column_to_play/2)-(get_last_col_played()/2);
        }

        while(is_added()==false) 
        {
            if(column_to_play+row>DEFAULT_WIDTH)
            {
                // row value used to pick the next available column to play if the chosen one is full
                // It will default to 0 if it gets too large
                row=WIDTH_START;
            }
            // Round is played virtually by the computer
            play_round("", column_to_play+row);
            // row will increase until token has been successfully played
            row++;
        }
    }
}

void replay_board_history(char * input_char)
{
    // Creating array for opening a previously saved game
    int board_history_opened[MAX_ROUNDS][DEFAULT_HEIGHT][DEFAULT_WIDTH];
    
    int input=0;

    while(input!=1 && input!=888 && input!=999)
    {
        puts("Please ensure that a valid file named 'board_history.bin' has been added to the '.\\open\\' directory.\nAre you ready to play? - type 1 to continue - type 888 to play Human vs Human mode - type 999 to play Human vs Computer mode - type 2 for more information");
        fgets(input_char, 20, stdin);
        sscanf(input_char, "%d", &input);
        
        if(input==2)
        {
            // Used to provide the user with help in getting the correct file in the correct place
            puts("\nA valid file is a file generated when playing a round of this game against another player or the computer. It is saved to the 'save' directory located within the same directory as this software file. You should ensure that one of these files is placed within the 'load' directory, also located within this directory. It should be named 'board_history.bin' or as it is named when generated.\n");
        }
    }

    // Offering the option to play a normal game
    if(input==888)
    {
        set_mode(MODE_HUMAN_HUMAN);
    }
    else if(input==999)
    {
        set_mode(MODE_COMPUTER_HUMAN);
    }
    else
    {
        while(!fopen(".\\open\\board_history.bin","rb") || input!=1)
        {
            // Announces if file not found in correct location
            puts("\nFile not found! Please add 'board_history.bin' to the '.\\open\\' directory. Type 1 when ready to proceed\n");
            fgets(input_char, 20, stdin);
            sscanf(input_char, "%d", &input);
        }
        // Once file can be found, program will read it and transfer it into a board_history_file_opened array
        FILE *board_history_file_open;
        board_history_file_open = fopen(".\\open\\board_history.bin","rb");
        fread(board_history_opened, sizeof(board_history_opened), 1, board_history_file_open);
        puts("Loading... \n");

        for(int round=0; round<MAX_ROUNDS; round++)
        {
            for(int height=HEIGHT_START; height<DEFAULT_HEIGHT; height++)
            {
                for(int width=WIDTH_START; width<DEFAULT_WIDTH; width++)
                {
                    // Copies board_history_opened into board_history
                    board_history[round][height][width] = board_history_opened[round][height][width];
                }
            }
        }
        // Removes the highest_round_num constraint for this mode
        highest_round_num=MAX_ROUNDS;

        for(int round=0; round<MAX_ROUNDS; round++)
        {
            show_board();
            // Waits 1 second between moves
            delay(1);
            // Runs redos until a winner is found
            redo(false);
            
            if(check_win(1) || check_win(2))
            {
                display_win(0);
                // Loop breaks when a winner has been found
                break;
            }
        }
         
    }
}